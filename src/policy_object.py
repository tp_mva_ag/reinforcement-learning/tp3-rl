import numpy as np


class GaussianPolicy(object):
    def __init__(self, theta, sigma):
        self.theta = theta
        self.sigma = sigma

    def draw_action(self, state):
        return np.random.normal(self.theta * state, self.sigma)

    def real_theta(self):
        return self.theta

    def get_sigma(self):
        return self.sigma


class UniformPolicy(object):
    def __init__(self, actions_list):
        self.actions_list = actions_list

    def draw_action(self, state):
        idx = np.random.randint(0, len(self.actions_list))
        return self.actions_list[idx]


class GreedyPolicy(object):
    def __init__(self, theta, actions_list):
        self.actions_list = actions_list
        self.theta = theta

    def draw_action(self, state):
        return np.argmax([
            np.dot(
                np.transpose(
                    np.array([act, state * act, (state ** 2) + (act ** 2)])),
                self.theta)
            for act in self.actions_list])

    def set_theta(self, theta):
        self.theta = theta
