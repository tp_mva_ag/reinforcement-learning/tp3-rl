import matplotlib.pyplot as plt
import src.utils as utils
from src.policy_object import *
from tqdm import tqdm


#####################################################
# Define the environment and the policy
#####################################################
# env = lqg1d.LQG1D(initial_state_type='random')


# real_theta = -0.6
# sigma = 0.4
# policy = GaussianPolicy(theta=real_theta, sigma=sigma)

# #####################################################
# # Experiments parameters
# #####################################################
# # We will collect N trajectories per iteration
# N = 50
# # Each trajectory will have at most T time steps
# T = 10
# # Number of policy parameters updates
# n_itr = 10
# # Set the discount factor for the problem
# discount = 0.9
# # Learning rate for the gradient update
# learning_rate = 0.01


#####################################################
# Define the update rule (stepper)
#####################################################


def stepper_invers(x, learning_rate):
    # e.g., constant, adam or anything you want
    return learning_rate / (x + 1)


def stepper_constant(_, learning_rate):
    # e.g., constant, adam or anything you want
    return learning_rate


def mu_theta(s, p_theta):
    # Probability original state
    return p_theta * s


def grad_mu_theta(s, _):
    # Probability original state
    return s


def compute_r(list_rewards, discount):
    r = 0
    for i in range(len(list_rewards)):
        r += (discount ** i) * list_rewards[i]

    return r


def grad_log(thet, a, s, sigm):
    return grad_mu_theta(s, thet) * \
           (a - mu_theta(s, thet)) / (sigm ** 2)


def grad_j(paths, nb_traj, thet, discount, sigma):
    grad_j_est = 0
    for n in range(nb_traj):
        # We get the lists of states, actions and rewards of this trajectory
        actions = paths[n]['actions']
        rewards = paths[n]['rewards']
        states = paths[n]['states']

        r = compute_r(rewards, discount)

        # For each state of this trajectory, we update the estimaiton of theta
        for t in range(len(states)):
            a = actions[t]
            s = states[t]
            # \nabla_{theta} log(\pi_{theta}(a|x_t))
            log_grad = grad_log(thet, a, s, sigma)

            grad_j_est += log_grad

            r = r / discount - rewards[t]  # Because r = sum{s=t->T}(r_t)

    grad_j_est /= nb_traj
    return grad_j_est


def get_bin_count(n_bin, min_st, max_st, min_act, max_act, state, action, grid_width):
    id_st = int(0.99 * grid_width * (state - min_st) / (max_st - min_st))
    id_act = int(0.99 * grid_width * (action - min_act) / (max_act - min_act))

    n_bin[id_st, id_act] += 1

    return n_bin[id_st, id_act]


# fill the following part of the code with
#  - REINFORCE estimate i.e. gradient estimate
#  - update of policy parameters using the steppers
#  - average performance per iteration
#  - distance between optimal mean parameter and the one at it k


# We realise n_itr times the experiment, to determine the mean of the theta est and std deviation around this value
def lqg_reinforce(env, policy, n_simu=10, n_traj=100, t_max=40,
                  stepper=stepper_invers, learning_rate=0.1, discount=0.9,
                  expl_bonus=False, expl_grid_width=10,
                  verbose=True):
    """ Estimates the value of theta with the LQG REINFORCE method.

    :param env: the simulation environment,
    :param policy: the policy to estimate
    :param n_simu: number of simulations
    :param n_traj: Number of trajectories by simulation
    :param t_max: Max number of steps by trajectory
    :param stepper: The function generating alpha_t
    :param learning_rate: The parameter of the stepper
    :param discount: THe discount or calculating R
    :param expl_bonus:
    :param expl_grid_width:
    :param verbose:

    :return: historic_theta: n_simu*N matrix, the historic of the values of theta
    :return: historic_diff: n_simu*N matrix, the historic of the differences between the real theta and the estimator
    """

    historic_theta = []
    historic_diff = []
    real_theta = policy.real_theta()

    # We do n_iter simulations to average the results
    for _ in range(n_simu):

        hist_theta_simu = np.zeros((n_traj * t_max, 1))
        hist_diff_simu = np.zeros((n_traj * t_max, 1))
        theta = np.random.uniform(-10, 10)

        # We do the simulation
        paths = utils.collect_episodes(env, policy=policy, horizon=t_max, n_episodes=n_traj)

        if expl_bonus:
            min_state = np.min([np.min(i['states']) for i in paths])
            max_state = np.max([np.max(i['states']) for i in paths])
            min_action = np.min([np.min(i['actions']) for i in paths])
            max_action = np.max([np.max(i['actions']) for i in paths])
            max_reward = np.max([np.max(i['rewards']) for i in paths])
            beta = max_reward / (1 - discount)  # TODO: verify what is the Omega function

            n_bin = np.zeros((expl_grid_width, expl_grid_width))

        # We go through each trajectory
        if verbose:
            in_range = tqdm(range(n_traj), "Trajectory %d over %d : " % (_ + 1, n_simu))
        else:
            in_range = range(n_traj)

        for n in in_range:
            # We get the lists of states, actions and rewards of this trajectory
            states = paths[n]['states']

            if expl_bonus:
                actions = paths[n]['actions']
                rewards = paths[n]['rewards']

                for i in range(len(rewards)):
                    rewards[i] += beta * np.sqrt(1 / get_bin_count(n_bin, min_state, max_state, min_action, max_action,
                                                                   states[i], actions[i], expl_grid_width))

            # For each state of this trajectory, we update the estimaiton of theta
            for t in range(len(states)):
                j = grad_j(paths, n_traj, theta, discount, policy.get_sigma())
                theta += stepper(t, learning_rate) * j

                hist_theta_simu[n * t_max + t] = theta
                hist_diff_simu[n * t_max + t] = real_theta - theta

        historic_theta.append(hist_theta_simu)
        historic_diff.append(hist_diff_simu)

    return historic_theta, historic_diff


def plot_results(historic_theta, historic_diff):
    # plot the average return obtained by simulating the policy
    # at each iteration of the algorithm (this is a rought estimate
    # of the performance

    mean_theta = np.average(np.array(historic_theta), axis=0)
    mean_diff = np.average(np.array(historic_diff), axis=0)

    std_theta = np.std(np.array(historic_theta), axis=0)
    std_diff = np.std(np.array(historic_diff), axis=0)

    plt.figure()
    plt.subplot(121)
    plt.plot(mean_diff, 'b')
    plt.plot(mean_diff - std_diff, 'b--')
    plt.plot(mean_diff + std_diff, 'b--')

    plt.title('Estimation of theta')
    plt.xlabel('Iterations')
    plt.ylabel('Theta')
    plt.grid(True)
    plt.legend(('Mean over simulations', 'Confidence interval'))

    # plot the distance mean parameter
    # of iteration k
    plt.subplot(122)
    plt.plot(mean_theta, 'r')
    plt.plot(mean_theta - std_theta, 'r--')
    plt.plot(mean_theta + std_theta, 'r--')

    plt.title('Error of the estimator')
    plt.xlabel('Iterations')
    plt.ylabel('Error')
    plt.grid(True)
    plt.legend(('Mean over simulations', 'Confidence interval'))

    n = len(mean_diff)
    half = int(n/2)
    x_list = range(half, len(mean_diff))

    plt.figure()
    plt.subplot(121)
    plt.plot(x_list, mean_diff[half:], 'b')
    plt.plot(x_list, mean_diff[half:] - std_diff[half:], 'b--')
    plt.plot(x_list, mean_diff[half:] + std_diff[half:], 'b--')

    plt.title('Estimation of theta\nZoom on the end')
    plt.xlabel('Iterations')
    plt.ylabel('Theta')
    plt.grid(True)
    plt.legend(('Mean over simulations', 'Confidence interval'))

    # plot the distance mean parameter
    # of iteration k
    plt.subplot(122)
    plt.plot(x_list, mean_theta[half:], 'r')
    plt.plot(x_list, mean_theta[half:] - std_theta[half:], 'r--')
    plt.plot(x_list, mean_theta[half:] + std_theta[half:], 'r--')

    plt.title('Error of the estimator\nZoom on the end')
    plt.xlabel('Iterations')
    plt.ylabel('Error')
    plt.grid(True)
    plt.legend(('Mean over simulations', 'Confidence interval'))

    plt.show()
