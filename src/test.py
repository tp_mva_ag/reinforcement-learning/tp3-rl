from src.mainTP3_PG import *
from src.mainTP3_FQI import *

############################################################
# PG #
############################################################

# hist_theta, hist_diff = lqg_reinforce(env=lqg1d.LQG1D(initial_state_type='random'),
#                                       policy=GaussianPolicy(theta=-0.6, sigma=0.4),
#                                       n_simu=10, n_traj=20, t_max=10, stepper=stepper_constant,
#                                       learning_rate=0.0001, discount=0.9,
#                                       expl_bonus=True)
#
# plot_results(hist_theta, hist_diff)

############################################################
# FQI #
############################################################

horizon = 50
N = 25  # This is the parameter with most impact on the duration if J is calculated
T = 100
n_simu = 5
lambd = 0.1

env, discrete_actions, discrete_states, sa = gen_env_sa()

gammas = np.linspace(0, 0.2, 3)
for gamma in gammas:
    q_opt = compute_q_opt(env, sa, gamma)
    historic_q, historic_j = fqi(env, discrete_actions, n_simu, N, T, gamma, lambd, sa, q_opt, compute_j=True)
    plot_fqi(historic_q, historic_j, mult_plot=True)
    plt.title('Gamma = %f' % gamma)

plt.show()
