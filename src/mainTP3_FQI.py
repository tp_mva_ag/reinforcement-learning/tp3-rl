import src.lqg1d as lqg1d
from src.policy_object import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from src.utils import collect_episodes, estimate_performance
from tqdm import tqdm


#################################################################
# Show the optimal Q-function
#################################################################
def make_grid(x, y):
    m = np.meshgrid(x, y, copy=False, indexing='ij')
    return np.vstack(m).reshape(2, -1).T


def q_fun_empirical(env, sa, discount):
    k, cov = env.computeOptimalK(discount), 0.001
    q_fun = np.vectorize(lambda s, a: env.computeQFunction(s, a, k, cov, discount, 1))

    return q_fun(sa[:, 0], sa[:, 1])


def q_fun_linear_est(sa, theta):
    q_fun_theta = np.vectorize(lambda s, a: np.array([[a, s * a, (s ** 2) + (a ** 2)]]).dot(theta))

    return q_fun_theta(sa[:, 0], sa[:, 1])


def gen_env_sa(act_lim=8, st_lim=10, grid_mesh=20):
    env = lqg1d.LQG1D(initial_state_type='random')
    discrete_actions = np.linspace(-act_lim, act_lim, grid_mesh)
    discrete_states = np.linspace(-st_lim, st_lim, grid_mesh)
    sa = make_grid(discrete_states, discrete_actions)

    return env, discrete_actions, discrete_states, sa


def compute_q_opt(env, sa, discount):
    return q_fun_empirical(env, sa, discount)


def plot_q(states, actions, q):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(states, actions, q)
    plt.show()


#################################################################
# Collect the samples using the behavioural policy
#################################################################
# You should use discrete actions


def fqi(env, discrete_actions_list, n_simu, n_traj, t_max, gamma, lambd, SA, Q_opt, compute_j=True):
    hist_q = np.zeros((n_simu, n_traj))
    hist_j = np.zeros((n_simu, n_traj))

    for idx_simu in range(n_simu):
        paths = collect_episodes(env, policy=UniformPolicy(discrete_actions_list),
                                 horizon=t_max, n_episodes=n_traj)

        theta = np.random.uniform(-1, 1, (3, 1))

        for idx_traj in tqdm(range(n_traj), 'Going through Dataset - %d over %d' % (idx_simu + 1, n_simu)):
            path_traj = paths[idx_traj]

            states = path_traj['states']
            next_states = path_traj['next_states']
            actions = path_traj['actions']
            rewards = path_traj['rewards']

            z_n = np.zeros((t_max, 3))
            y_n = np.zeros((t_max, 1))
            for t in range(t_max):
                action = actions[t]
                state = states[t]
                next_state = next_states[t]
                reward = rewards[t]

                phi = np.array([action, action * state, (state ** 2) + (action ** 2)])
                z_n[t] = phi

                list_q_act = q_fun_linear_est(make_grid(next_state, discrete_actions_list), theta)
                tq = reward + gamma * np.max(list_q_act)

                y_n[t] = tq

            z_z_inv = np.linalg.inv(z_n.transpose().dot(z_n) + lambd * np.eye(3))
            z_y = z_n.transpose().dot(y_n)
            theta = z_z_inv.dot(z_y)

            q_n = q_fun_linear_est(SA, theta)
            hist_q[idx_simu, idx_traj] = np.sqrt(np.sum((q_n - Q_opt) ** 2))
            if compute_j:
                j = estimate_performance(env, GreedyPolicy(theta, discrete_actions_list),
                                         horizon=t_max, n_episodes=n_traj, gamma=gamma)
                hist_j[idx_simu, idx_traj] = j

    return hist_q, hist_j


def plot_fqi(hist_q, hist_j=None, mult_plot =False):
    mean_q = np.average(np.array(hist_q), axis=0)
    std_q = np.std(np.array(hist_q), axis=0)

    plt.figure()
    plt.plot(mean_q)
    plt.plot(mean_q - std_q, 'b--')
    plt.plot(mean_q + std_q, 'b--')

    plt.title('Error of the estimator')
    plt.xlabel('Iterations')
    plt.ylabel('Error')
    plt.grid(True)
    plt.legend(('Mean over simulations', 'Confidence interval'))

    if hist_j is not None:
        mean_j = np.average(np.array(hist_j), axis=0)
        std_j = np.std(np.array(hist_j), axis=0)
        plt.figure()
        plt.plot(mean_j)
        plt.plot(mean_j - std_j, 'b--')
        plt.plot(mean_j + std_j, 'b--')

        plt.title('Estimation of J')
        plt.xlabel('Iterations')
        plt.ylabel('J')
        plt.grid(True)
        plt.legend(('Mean over simulations', 'Confidence interval'))

    if not mult_plot:
        plt.show()
